<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Register</title>
</head>
<body>
<h1>Buat Account Baru!</h1>
    <h3>Sign Up Form</h3>
    <form action="welcome" method="POST">
        @csrf
        <label>First Name:</label> <br><br>
        <input type="text" name="firstname"> <br><br>
        <label>Last Name:</label> <br><br>
        <input type="text" name="lastname"> <br><br>
        <label>Gender:</label> <br><br>
        <input type="radio" id="male" name="gender" value="male"> Male <br>
        <input type="radio" id="female" name="gender" value="female"> Female <br>
        <input type="radio" id="other" name="gender" value="other"> Other <br><br>
        <label>Nationality:</label> <br><br>
        <select name="Nationality">
            <option value="Indonesian">Indonesian</option>
            <option value="Singaporean">Singaporean</option>
            <option value="Malaysian">Malaysian</option>
            <option value="Australian">Australian</option>
        </select> <br><br>
        <label>Language Spoken:</label> <br><br>
        <input type="checkbox" id="Bahasa Indonesia" name="language" value="Bahasa Indonesia"> Bahasa Indonesia <br>
        <input type="checkbox" id="English" name="language" value="English"> English <br>
        <input type="checkbox" id="Other" name="language" value="Other"> Other <br><br>
        <label>Bio:</label> <br><br>
        <textarea name="message" rows="10" cols="30">
        </textarea> <br>
        <input type="submit" value="Sign Up">
    </form>
</body>
</html>