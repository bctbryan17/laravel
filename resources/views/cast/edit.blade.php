@extends('master')

@section('title')
    Halaman Tambah Cast
@endsection

@section('subtitle')
    Halaman Tambah Cast
@endsection

@section('content')

<div>
        <form action="/cast/{{$cast->id}}" method="POST">
            @method('put')
            @csrf
            <div class="form-group">
                <label>Nama Cast</label>
                <input type="text" class="form-control" name="nama" value="{{$cast->nama}}" placeholder="Masukkan Nama Cast">
                @error('nama')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label>Umur Cast</label>
                <input type="integer" class="form-control" name="umur" value="{{$cast->umur}}" placeholder="Masukkan Umur Cast">
                @error('umur')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label>Bio Cast</label>
                <input type="text" class="form-control" name="bio" value="{{$cast->bio}}" placeholder="Masukkan Bio Cast">
                @error('bio')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <button type="submit" class="btn btn-primary">Tambah</button>
        </form>
</div>

@endsection